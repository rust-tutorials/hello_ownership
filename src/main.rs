fn main() {
    println!("Hello, ownership");

//  // Scoping
    let x = "unscoped var";
    //scope blocks
    { // start scope
        let x = "scoped var";
        println!("  scope: {}", x); //shadowing "unscoped var"
    } // end scope <--- the scoped x is destroyed here
    println!("  scope: {}", x);



//  // Ownership
    let x = vec![0];
    let y = x; // ownership is passed by assignment 
    // println!("  ownership: {}", x[0]); // fails

    fn xy(_: Vec<i32>) {}
    xy(y); //ownership is passed by default to functions
    // println!("  ownership: {}", y[0]); // fails

    fn xz(z: Vec<i32>) -> Vec<i32> {z} // return the argument
    let x = vec![9];
    let x = xz(x); // re-assign passed argument
    println!("  ownership: {}", x[0]); // works
    // this is the manual version of borrowing



//  // Borrowing
    fn z(_: &Vec<i32>) {} //equivilent of manual borrow above
    z(&x);
    fn immutable(x: &Vec<i32>) {
        // x.push(1); // immutable
        println!("  borrowing: {}", x[0]); // works
    }
    immutable(&x);

    let mut m = vec![7];
    fn mutable(x: &mut Vec<i32>) {
        x.push(1); // immutable
        println!("  mutable: {}", x[1]); // works
    }
    mutable(&mut m);
    {
        let mut_ref = &mut m; // this borrow must finish
        mut_ref[0] += 1;      // before we can use m again
        println!("  mutable ref: {}", mut_ref[0]);
    }
    m[0] += 1; // scoping prevents two simultanious borrows
    println!("  mutable ref: {}", m[0]);

    for val in m { // for loop causes a borrow or move
        println!("  loops borrow: {}", val);
        // m.push(666);
        // mutable borrow not possible in loop scope
    }



//  // Lifetimes
//    let r;
//    {
//        let i = 9;
//        r = &i;
//    } // <--- i dropped while still borrowed
//    println!("  dangling reference: {}", r);

    fn use_one(_: &str) -> &str {
        ""
    }
//    fn use_either(str1: &str, str2: &str) -> &str {
//        ""     // ^---- Requires lifetime parameters
//    }

    fn use_either<'a, 'b, 'c>(_: &'a str, _: &'b str) -> &'c str {
        ""     // ^---- Independant lifetimes
    }
    println!("  lifetimes: {} {}", use_one(""), use_either("", ""));

    fn use_first<'a, 'b>(a: &'a str, _: &'b str) -> &'a str {
        a     // ^---- Lifetime tied to first param
    }
    fn use_second<'a, 'b>(_: &'a str, b: &'b str) -> &'b str {
        b     // ^---- Lifetime tied to second param
    }
    println!("  lifetimes: {} {}", use_first("", ""), use_second("", ""));

    struct VectorBag<'a> {
        compartment: &'a Vec<i32>,
        pouch: &'a Vec<i64>
    }
    let threes = vec![3];
    let fives = vec![5];
    let odd_bag = VectorBag{compartment: &threes, pouch: &fives};

    // monkey patch a function onto the struct
    impl<'a> VectorBag<'a> {
        fn get_first(&self) -> &'a i64 {
            &self.pouch[0]
        }
    }
    println!("  3s and 5s: {} {}", odd_bag.compartment[0], odd_bag.get_first());

    static INVARIANT: i64 = 42;
    let static_ref: &'static i64 = &INVARIANT;
    println!("  static lifetime: {}", static_ref);



//  // Mutability
    let immut = 5;
    // immut += 1; // fails - immutability by default
    let mut muta = 6;
    muta += 1;
    println!("  (im)mutability: {} {}", immut, muta);

    let muta_ref = &mut muta;
    *muta_ref += 1;
    let mut another = 1; another += 1;
//    muta_ref = &mut another; // fails - ref is not mutable

    // mutable references
    let mut mutee = 2;
    let mut another_muta_ref = &mut mutee; // mutable ref to mutable data
    println!("  mutabile refs: {}", another_muta_ref);
    another_muta_ref = &mut another; // re assign reference
    println!("  mutabile refs: {} {}", muta_ref, another_muta_ref);

    use std::cell::Cell;
    // partial mutability
    struct Point {
        x: i64,
        // mut y: i64 // not allowed
        // y: &'a Vec<i64> // not mutable
        y: Cell<i64>,
    }
    let p = Point{x: 2, y: Cell::new(4)};
    p.y.set(6);
    println!("  partial mutability: {} {:?}", p.x, p.y);
    println!("  array printing: {:?}", vec![1,2,3]);


}
